# tap-covid19

This is a [Singer](https://singer.io) tap that produces JSON-formatted data
following the [Singer
spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md).

This tap:

- Pulls raw data from https://disease.sh/
- Extracts the following resources:
  - (https://disease.sh/v3/covid-19/historical?lastdays=<last_days>)
- Outputs the schema for each resource
- Incrementally pulls data based on the input state

---

Copyright &copy; 2018 Stitch

#!/usr/bin/env python3
import os
import json
import singer
from singer import utils, metadata
from singer.catalog import Catalog, CatalogEntry
from singer.schema import Schema
import time
import datetime
import requests


DATE_FORMAT_API="%m/%d/%y"
DATE_FORMAT_CONFIG ="%Y-%m-%d"

API_URI = 'https://disease.sh/v3/covid-19/historical/'


REQUIRED_CONFIG_KEYS = ["start_date"]
LOGGER = singer.get_logger()

def retrieve_data(schema, config, state):
    if schema == 'covid19_timeseries_country':
        today = datetime.date.today()
        from_date_str = ''
        if schema in state:
            from_date_str = state[schema]
            from_date = datetime.datetime.strptime(from_date_str, '%Y-%m-%dT%H:%M:%SZ').date()
        else:
            from_date_str = config['start_date'] 
            from_date = datetime.datetime.strptime(from_date_str, '%Y-%m-%d').date()
        
        
        last_days = (today - from_date).days
        
        if last_days == 0:
            return []
        
        if last_days < 0:
            last_days = 1
        
        payload = {'lastdays': last_days}    

        res = requests.get(API_URI , params=payload)
        
        if res.status_code >= 400:
            LOGGER.log_exception('HTTP Request returned 4XX/5XX status code')
            return []
        
        countries = res.json()

        if type(countries) is not list:
            countries = [countries]
        data = []
        temp = dict()
        for country in countries:
            if country != None:
                record = dict()
                record["country"] = country["country"]
                record["province"] = country["province"]
                for (_date, cases), (_d, deaths), (_r, recovered) in zip(country['timeline']['cases'].items(),\
                    country['timeline']['deaths'].items(), country['timeline']['recovered'].items()):
                    _date = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.strptime(_date, DATE_FORMAT_API))
                    record["date"] = _date
                    record["cases"] = cases
                    record["recovered"] = recovered
                    record["deaths"] = deaths
                    if temp.get(_date) is None:
                        temp[_date] = [dict(record)]
                    else:
                        temp[_date].append(dict(record))

        for _date in temp.keys():
            data.append(list(temp[_date]))
        
        return data


def get_abs_path(path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), path)


def load_schemas():
    """ Load schemas from schemas folder """
    schemas = {}
    for filename in os.listdir(get_abs_path('schemas')):
        path = get_abs_path('schemas') + '/' + filename
        file_raw = filename.replace('.json', '')
        with open(path) as file:
            schemas[file_raw] = Schema.from_dict(json.load(file))
    return schemas


def discover():
    raw_schemas = load_schemas()
    streams = []
    for stream_id, schema in raw_schemas.items():
        stream_metadata = [{
			"metadata":{
				"selected": True
			},
			"breadcrumb": []
		}]
        key_properties = []
        rep_key = 'date'
        streams.append(
            CatalogEntry(
                tap_stream_id=stream_id,
                stream=stream_id,
                schema=schema,
                key_properties=key_properties,
                metadata=stream_metadata,
                replication_key=rep_key,
                is_view=None,
                database=None,
                table=None,
                row_count=None,
                stream_alias=None,
                replication_method=None,
            )
        )
    return Catalog(streams)


def sync(config, state, catalog):
    """ Sync data from tap source """
    # Loop over selected streams in catalog
    
    for stream in catalog.get_selected_streams(state):
        LOGGER.info("Syncing stream:" + stream.tap_stream_id)


        bookmark_column = stream.replication_key
        is_sorted = True
        singer.write_schema(
            stream_name=stream.tap_stream_id,
            schema=stream.schema.to_dict(),
            key_properties=stream.key_properties,
        )

        tap_data = retrieve_data(stream.tap_stream_id, config, state)
        
        for row in tap_data:
            # write one or more rows to the stream:
            singer.write_records(stream.tap_stream_id, row)
            if bookmark_column:
                if is_sorted:
                    singer.write_state({stream.tap_stream_id: row[0][bookmark_column]})
                else:
                    # if data unsorted, save max value until end of writes
                    max_bookmark = max(max_bookmark, row[0][bookmark_column])
        

        today = datetime.datetime.combine(datetime.date.today(), datetime.datetime.min.time())
        today = today.strftime('%Y-%m-%dT%H:%M:%SZ')

        if bookmark_column and is_sorted:
            singer.write_state({stream.tap_stream_id: today})

        if bookmark_column and not is_sorted:
            singer.write_state({stream.tap_stream_id: max_bookmark})

    return  


@utils.handle_top_exception(LOGGER)
def main():
    # Parse command line arguments
    args = utils.parse_args(REQUIRED_CONFIG_KEYS)
    # If discover flag was passed, run discovery mode and dump output to stdout
    if args.discover:
        catalog = discover()
        catalog.dump()
    # Otherwise run in sync mode
    else:
        if args.catalog:
            catalog = args.catalog
        else:
            catalog = discover()
        sync(args.config, args.state, catalog)


if __name__ == "__main__":
    main()
